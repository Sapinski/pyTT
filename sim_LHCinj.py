# -*- coding: utf-8 -*-

'''

    This is a main simulation file for the case of the flying wire.
    It defines the type of simulation.
    To run: > python sim.py
    2016.08.16 m.sapinski@gsi.de

'''

from modules import ttbeam, ttarget, ttsim, ttout
import math
import numpy
import matplotlib.pyplot as plt

# define simulation type
ttsim.case = "WireScanner"  # propagate to ttarget

#
# define the beam
# LHC at injection case from the main paper
ttbeam.sigx = 0.53
ttbeam.sigy = 0.8
ttbeam.npart = 0.25*3.2e14  # total intensity of the circulating beam

# overwrite default target definition:
ttarget.depth = 0.03    # [mm] wire thickness
ttarget.vw = 1000       # [mm/s] wire speed

# overwrite the end of the scan position:
ttsim.endScan=10 * ttbeam.sigy

# overwrite which cooling processes should be executed:
ttsim.radiativeCooling=True
ttsim.conductiveCooling=False


#print "testing, the total beam intensity:"
#print ttbeam.getPartOnWireCell(0., 0., 20.0, 20.0)[0]

#print "plotting the 2D beam transverse profile:"
#ttbeam.plot_beam()

print "running the simulation:"
ttsim.runWireScanner()

#ttout.plotMaxTempEvol('out1.png')

print max(ttarget.eAccNparts)
# print ttarget.eAccTemp
print len(ttarget.xPos), len(ttarget.eAccNparts), len(ttsim.nparts)
# print len(ttarget.xPos),len(ttarget.eAccTemp)
# print wirePositions
# plt.plot(ttarget.xPos,ttarget.eAccTemp)
# plt.show()
