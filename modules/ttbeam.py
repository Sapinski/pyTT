'''

 This module describes particle beam for the need of thin target modelling.

 Input units: mm, g, s, MeV, MeV/u

 2016.08.15 by m.sapinski@gsi.de 

'''

import numpy
import matplotlib.pyplot as plt

from numpy.random import uniform #, seed
from scipy.interpolate import griddata
from matplotlib import cm
from scipy.integrate import dblquad

# import ttarget


# setup, maybe move to configuration file
# these are default values, they are normally changed in main simulation script
sigx=0.5          # [mm] horizontal beam size
sigy=0.3          # [mm] vertical beam size
trev=89e-6        # [s] beam revolution period (useful for synchrotrons and wire scanners
                  # [s] beam repetition period
npart=1.1e11      # number of particles in the bunch/pulse
pulseLength=50e-6 # [s] length of the pulse (for pulsed beam) or bunch, if bunch effects considered
ene=450e3         # [MeV] - beam energy, not used               NOT USED FOR NOW
# comment: stoping power is a property of projectile and target, but projectile
# energy plays usually more important role, so it is placed in ttbeam module.
# Convinient way to get stopping powers for protons is pstar database:
# http://physics.nist.gov/cgi-bin/Star/ap_table.pl
dedx=2.0          # [MeV*cm2/g] - typical units
dEdx=dedx*100     # as above but in [MeV*mm2/g]

# beam center is by definition in (0,0) 
# tested, integral gives 1.0
def gaus2d(x,y):
    '''
    Simple function returning the gaussian normalized to full beam intensity (np).
    Unit of the returned value is: [1/mm2]. After inttegration gives numer of particles.
    '''
    a = npart/(2*numpy.pi*sigx*sigy)
    g = a*numpy.exp(-0.5*((x/sigx)**2 +(y/sigy)**2))
    return g


def gaus2d_flux(x,y):
    '''
    Returns gaussian normalized to particle flux in synchrotron.
    Unit of the eeturned value is [1/mm2s], after integration it gives number of particles per second.
    
    '''    
    a=npart/(2*numpy.pi*sigx*sigy*trev)
    g = a*numpy.exp(-0.5*((x/sigx)**2 +(y/sigy)**2))
    return g


# contour plotting function from:
# http://stackoverflow.com/questions/26999145/matplotlib-making-2d-gaussian-contours-with-transparent-outermost-layer
# to tools?
def plot_countour(x,y,z):
    # define grid.
    xi = numpy.linspace(-5*sigx, 5*sigx, 100)  # make arrays depend on beam sigma
    yi = numpy.linspace(-5*sigy, 5*sigy, 100)  #
    ## grid the data.
    zi = griddata((x, y), z, (xi[None,:], yi[:,None]), method='cubic')
    # contour the gridded data, plotting dots at the randomly spaced data points.
    CS = plt.contour(xi,yi,zi,6,linewidths=0.5,colors='k')
    #CS = plt.contourf(xi,yi,zi,15,cmap=plt.cm.jet)
    CS = plt.contourf(xi,yi,zi,6,cmap=cm.Greys_r)
    #plt.colorbar() # draw colorbar
    # plot data points.
    plt.xlim(-5*sigx,5*sigx)
    plt.ylim(-5*sigy,5*sigy)
    # plt.title('griddata test (%d points)' % npts)
    plt.show()


def plot_beam():
    # make up some randomly distributed data
    #seed(1234)
    npts = 2000
    print 'plotting transverse profile of the beam with sigmax = ',sigx
    x = numpy.random.uniform(-5*sigx,5*sigy,npts)
    #x = numpy.random.uniform(ttsim.startScan,ttsim.endScan,npts)
    y = numpy.random.uniform(-5*sigx,5*sigy,npts)
    #z = gauss2d(x,y,Sigma=np.asarray([[1.,.5],[0.5,1.]]),mu=np.asarray([0.,0.]))
    z=map(gaus2d,x,y)
    plot_countour(x,y,z)
    
    
# 2do: modify function name    
def getPartOnWireCell(ay,ax,d=None,dh=None):
    '''
    Returns number of beam particles for a wire case. Works for case of SEM grid and beam pulse.
    Arguments are:
     y-[mm] distance of the wire from the beam center 
     x-[mm] distance of the wire element from the wire center
     d-[mm] wire diamterer
     dh-[mm] length of the wire element
    '''
    if d==None:
        import ttarget
        d=ttarget.depth
    if dh==None:
        import ttarget
        dh=ttarget.dh
    npts=dblquad(gaus2d,ay-d/2,ay+d/2, lambda x: ax-dh/2, lambda x: ax+dh/2, epsrel = 1e-9, epsabs = 0)
    return npts[0]


def getPartFluxOnWireCell(ay,ax,d=None,dh=None):
    '''
    Returns number of beam particles per second for a wire case. Works for case of Flying Wires and circulating beam.
    Arguments are:
     ay-[mm] distance of the wire from the beam center
     ax-[mm] distance of the wire element from the wire center
     d-[mm] wire diamterer
     dh-[mm] length of the wire element
     scipy.integrate.dblquad - the return value is a tuple, with the first element holding the estimated value of the
     integral and the second element holding an upper bound on the error
    '''
    if d==None:
        import ttarget
        d=ttarget.depth
    if dh==None:
        import ttarget
        dh=ttarget.dh
    npts=dblquad(gaus2d_flux,ay-d/2,ay+d/2, lambda x: ax-dh/2, lambda x: ax+dh/2, epsrel = 1e-9, epsabs = 0)
    return npts[0]

