'''

 This module describes target for the need of thin target modelling.

 2016.08.15 by m.sapinski@gsi.de 
 2016.10.06: move sublimation material constant here from ttphys.
 2017.06.30: submitting this version to gitlab (+ small corrections)

'''

import numpy, math
import ttbeam, ttphys
from scipy.interpolate import interp1d


case = 'WireScanner'  # other options: 'Screen', 'Grid'


# simple set and get functions make no sense
def setCase(incase):
    case = incase


dim = 1
if case == 'Screen':
    dim = 2

# common for all cases
# dw  = 0.03             # [mm] - target thickness, in case of wire-diameter
# rho = 2e-3             # [g/mm3] - target density
# wFun= 4.5              # [eV] work function for graphite   2do: temperature-depndent work function!!!
# wfun=wFun*ttphys.Qe    # [J] ---||----

material='C'             # default material
surface_material='C'     # sometimes surface material is different than bulk material

depth = 0.03  # [mm] - target thickness, in case of wire-diameter - replacing dw
density = 2.e-3  # [g/mm3] - target material density - replacing rho
awFun = 4.5  # [eV] auxiliary work function for graphite   2do: temperature-depndent work function!!!
wfun = awFun * ttphys.Qe  # [J] ---||----

# 1D case, moving of the wire:
vw = 1000  # [mm/s] wire speed
dh = 2.e-3  # [mm] bin size along the wire

emissivity = 0.5  # [no unit] radiative emissivity

molarMass=12.01  # [g/mol] for Carbon, important for sublimation cooling formula
vapHeat = 715000  # [J / mol] vaporization heat for Carbon

################################################################################################
# lists describing the target wire slicing and initial conditions (surfaces and volumes):
# these initialisations are good for wires (for now)
# this should be recalculated, right?
#xPos = numpy.arange(-4.0 * ttbeam.sigx, 4.0 * ttbeam.sigx - dh, dh)  # list of bin centers
                                            # (other proposed names: exPos, ePosX, cPosX, for cell)
xPos=list()
#eRad = [depth / 2 for i in range(len(xPos))]  # initial radiuses of wire elements (or mean size of pie-shaped screnne elements)
eRad=list()
#eVol = [math.pi * depth ** 2 * dh / 4 for i in range(len(xPos))]  # initial volumes of target elements,
eVol=list()

#eSurf = [math.pi * depth * dh for i in  range(len(xPos))] # initial values of the list of elements' external surfaces (radiating surface)
eSurf=list()
#eCSurf = [math.pi * depth **2/4 for i in  range(len(xPos))]# list of cross-section surfaces (conductive surfaces)
eCSurf=list()

#############################
# lists describing target wire slices condition in each simulation step:
eAccNparts = list()  # list of numers of particles accumulated in each target/wire element
eTemp = list()  # list of temperatures in cells/fragments
eThCurr=list()  # list of thermionic currents from cells/fragments
#eTemp=[300.0 for i in range(len(xPos))]     # room temperature state at the beginning



def init():
    '''
    need to reinitialize the target
    :return:
    '''
    global xPos
    global eRad
    global eVol
    global eSurf
    global eCSurf
    global eTemp
    global eThCurr
    xPos = numpy.arange(-4.0 * ttbeam.sigx, 4.0 * ttbeam.sigx - dh, dh)  # list of bin centers
                                            # (other proposed names: exPos, ePosX, cPosX, for cell)
    eRad = [depth / 2 for i in range(len(xPos))]  # initial radiuses of wire elements (or mean size of pie-shaped screnne elements)
    eVol = [math.pi * depth ** 2 * dh / 4 for i in range(len(xPos))]  # initial volumes of target elements,

    eSurf = [math.pi * depth * dh for i in  range(len(xPos))] # initial values of the list of elements' external surfaces (radiating surface)
    eCSurf = [math.pi * depth **2/4 for i in  range(len(xPos))]# list of cross-section surfaces (conductive surfaces)
    eTemp=[300.0 for i in range(len(xPos))]     # room temperature state at the beginning
    eThCurr = [ 0.0 for i in range(len(xPos)) ]  # room temperature state at the beginning
    # sublimation coefficients (according to Dushman book, "Scientific foundations of vacuum technique", Wiley Inc., New York 1966.):
    # and second edition from 1962
    global Sublimation_C1
    global Sublimation_C2
    if surface_material == 'C':
        Sublimation_C1 = 40.03e+3
        Sublimation_C2 = 12.04
    elif surface_material == 'W':
        Sublimation_C1 = 40.68e+3
        Sublimation_C2 = 9.30
    elif surface_material == "Au":
        Sublimation_C1 = 17.58e+3
        Sublimation_C2 = 8.80
    elif surface_material == "Cu":
        Sublimation_C1 = 16.98e+3
        Sublimation_C2 = 8.63
    elif surface_material == "Ti":
        Sublimation_C1 = 23.23e+3
        Sublimation_C2 = 9.11
    else:  # if problem we come back to failsafe carbon
        Sublimation_C1 = 40.03e+3
        Sublimation_C2 = 12.04


def getSHeat(material="C", temp=300):
    if material=="C":
        return getSHeat_C(temp)
    elif material=="W":
        return getSHeat_W(temp)
    else:
        print "target material not specified"



def getSHeat_C(temp=300):
    '''
    Function describing the specific heat of the material (graphite) as a function
    of the temperature. Default value of the temperature is 273 K.
    Function from original WireTemp2D.C. Return specific heat in J/gK.
    '''
    a1 = 2.97
    a2 = -35.1
    a3 = 0.49
    b1 = -0.012
    b2 = 0.0026

    cf = a1 + (a2 / pow(temp, a3))
    cfl = b2 * temp + b1

    ext_fact = 1.0
    if temp < 5:
        return ext_fact * 1e-3
    elif temp >= 5 and temp < 250.:
        return ext_fact * cfl
    elif temp >= 250:
        return ext_fact * cf


def getSHeat_W(temp=300):
    '''
    Function describing the specific heat of the material (tungsten) as a function
    of the temperature. Default value of the temperature is 300 K.
    Returns specific heat in J/gK.
    Use 1d interpolation on some data (in the future get better data!)
    '''
    temper=[298.0, 300.0, 1500.0, 2000.0, 2500.0, 3000.0, 3500.0, 3700.0]
    Sheat=[0.134, 0.134, 0.147, 0.169, 0.180, 0.196, 0.283, 0.337]
    finter = interp1d(temper, Sheat, kind='cubic')
    # print 'asked for temp = ', temp
    return finter(temp)


def getThConductivity(material="C",temp=300):
    '''
    :Parameters - material and temperature in Kelvin [K]: valid from 300K to sublimation temperature.
    '''
    if material=="C":
        return getThConductivity_C(temp)
    elif material=="W":
        return 2*getThConductivity_C(temp)  # preliminary!
    else:
        print "unknown material!"




# in the future to ttmaterial?
# to do: values below 300K (exist in WireTemp2D.C)
def getThConductivity_C(temp=300):
    '''
    :Parameter - temperature in Kelvin [K]: valid from 300K to sublimation temperature.
    :Returns - graphite thermal conductivity in [W/(mm*K)]:
    '''
    p0=1.19899
    p1=-9.06797e-04
    p2= 3.2114e-07
    p3=-3.93710e-11
    val1=p0+(p1*temp)+(p2*temp**2)+(p3*pow(temp,3))
    val1=val1/10   # needed in order to have mm in units
    if temp<359:
        return 0.0913
    else:
        return val1



