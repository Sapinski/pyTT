# -*- coding: utf-8 -*-

'''

    This is a main simulation file for the case of the SEM grid for GSI UNILAC.
    The beam particles are 238U with energy 5 MeV/u .
    These particles are stopped completly inside the grid wires (actually within microns,
    so a full 3D simulation is recommended)

    To run: > python sim_GSI_UNILAC_SEM.py
    2017.06.29, m.sapinski@gsi.de

    remark (2017.07.06): this is a test case for new simulation procedure ttsim.runLinacSEMgrid
    

'''

from modules import ttbeam, ttarget, ttsim, ttout, ttphys
import math


##########################################################
#               define simulation type/title             #
##########################################################
# it is SEM grid case
ttsim.case = "SEMGrid"  # not used
ttsim.title="GSI UNILAC SEM grid"  # use for printout


##########################################################
#                   define the beam                      #
#               UNILAC 5 MeV Uranium (4+)                #
#   beam size from BIF GUS4DK7, probably it is larger    #
#   downstream                                           #
##########################################################
ttbeam.sigx = 5.0 # [mm]
ttbeam.sigy = 1.5 # [mm]
ttbeam.npart = 1e12       # lets assume that trafos were reading 1 mA (intensity interlock gives actually
                          # a stronger limit)
ttbeam.trev = 0.02        # [s] repetition period (frequency 50 Hz)
ttbeam.pulseLength = 50.0e-6     # [s] pulse length, 50 us
ttbeam.tscan=10.0          # [s] duration of simulation (this should be longer probably)

##########################################################
#               define the target/detector               #
#                   100 um tungsten wire                 #
##########################################################

ttarget.depth = 0.1      # [mm] wire diameter
ttarget.dh = 0.1         # [mm] size of the bin along the wire

# target is 100 um thick gold-platted tungsten wire
ttarget.material='W'  # tungsten
ttarget.surface_material='Au'
ttarget.density = 19.25e-3  # [g/mm3]
ttarget.molarMass = 183.84
ttarget.awFun = 5.1                       # [eV], wikipedia says: 5.1 – 5.47 eV
ttarget.wfun = ttarget.awFun * ttphys.Qe  # work function in Joules, as needed
# from http://www.engineeringtoolbox.com/emissivity-coefficients-d_447.html
ttarget.emissivity=0.47   # not polished, maybe this value is too high?

ttarget.dEdx = 10.0/(ttarget.density*ttarget.depth) # [MeV*cm2/g], assumes 5 MeV beam


ttarget.init()  # need to run this, initialize target binning
print 'number of bins along the wire = ',len(ttarget.xPos)


##########################################################
#       define the rest of the simulation parameters     #
##########################################################
# the beginning and the end of the scan position:
#ttsim.startScan = 0.  # [mm] whole scan is 100 steps so 10 mm
#ttsim.endScan = 0.15    # [mm]
#ttsim.dt=1.0    # time step is 1 s, because this is step of the wires movement, but sub-steps are used inside
# above is wrong!!! overestimates the cooling
ttsim.dt=0.5*ttbeam.pulseLength

# overwrite which cooling processes should be executed:
ttsim.radiativeCooling=True
ttsim.thermionicCooling=True
ttsim.conductiveCooling=True

#########################################################
#   run tests
print "testing, the energy deposit with the first shot:"
print ttbeam.getPartFluxOnWireCell(5., 0.)

##########################################################
#           run main simulation                          #
##########################################################
print "running the simulation:"
ttsim.runLinacSEMgrid()


#########################################################
# analyse/save/plot output
ttout.plotMaxTempEvol('GSI_UNILAC_10s_20170706.png')

#ttout.plotMaxThCurrEvol('JPARC_MWPM_Ith_prof_5.png')

#ttout.saveMaxThCurrEvol("JPARC_MWPM_ThCurrEvol_5.csv")
