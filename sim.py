# -*- coding: utf-8 -*-

'''

    This is a main simulation file for the case of the moving SEM grid for J-PARC MCR injection.
    Concerned device names are: MWPM 3,4,5.
    The injected particles are H-. The dE/dx is calculated as a sum of dEdx of a proton and 2 electrons.

    To run: > python sim.py
    2016.09.29 m.sapinski@gsi.de

    remark: for the moment no tilt wrt.the beam is taken into account

'''

from modules import ttbeam, ttarget, ttsim, ttout, ttphys
import math
import numpy
import matplotlib.pyplot as plt

# define simulation type
# it is wire scanner type,but in fact it is a slowly moving (a step per beam pulse) SEM grid
ttsim.case = "WireScanner"  # or mixed? MovingGrid

#
# define the beam
# J-PARC 400 MeV linac
ttbeam.sigx = 1.3 # [mm]
ttbeam.sigy = 1.5 # [mm]
ttbeam.npart = 0.001*100*8e13  # 100 beam pulses assumed, pulse frequency: 1 Hz; 8e13 is maximum intensity, task here is
                         # to find which fraction of the maximumintensity can be safely scanned
                         # each pulse lasts 50 us, the rest is time for cooling
ttbeam.trev = 1.0        # [s] repetition period
ttbeam.pulseLength = 50.0e-6     # [s] pulse length


# stopping power of 400 MeV protons in tungsten (electrons immediately dissociate and their energy can be neglected):
ttarget.dEdx=1.1631e2    # [MeV*mm2/g], from http://physics.nist.gov/cgi-bin/Star/ap_table.pl

# overwrite default target definition:
ttarget.depth = 0.1    # [mm] wire thickness
ttarget.vw = 0.1       # [mm/s] one stepper second

# target is 100 um thick gold-platted tungsten wire
ttarget.density = 19.25e-3  # [g/mm3]
ttarget.molarMass = 183.84
ttarget.awFun = 5.1                       # [eV], wikipedia says: 5.1 – 5.47 eV
ttarget.wfun = ttarget.awFun * ttphys.Qe  # work function in Joules, as needed
# from http://www.engineeringtoolbox.com/emissivity-coefficients-d_447.html
ttarget.emissivity=0.47   # not polished, maybe this value is too high?

# the beginning and the end of the scan position:
ttsim.startScan = -5.0  # [mm] whole scan is 100 steps so 10 mm
ttsim.endScan = 5.0
ttsim.dt=1.0    # time step is 1 s, because this is stepof the wires movement

# overwrite which cooling processes should be executed:
ttsim.radiativeCooling=True
ttsim.conductiveCooling=False

print "testing, the energy deposit with the first shot:"
print ttbeam.getPartFluxOnWireCell(5., 0.)

print "plotting the 2D beam transverse profile:"
ttbeam.plot_beam()

print "running the simulation:"
ttsim.runLinacWireScanner()

#ttout.plotMaxTempEvol('out1.png')

print max(ttarget.eAccNparts)
# print ttarget.eAccTemp
print len(ttarget.xPos), len(ttarget.eAccNparts), len(ttsim.nparts)
# print len(ttarget.xPos),len(ttarget.eAccTemp)
# print wirePositions
# plt.plot(ttarget.xPos,ttarget.eAccTemp)
# plt.show()
